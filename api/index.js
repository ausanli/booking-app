const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
const cookieParser = require("cookie-parser");
require("dotenv").config();
const assetsRouter = require("./routes/assets");
const userRouter = require("./routes/user");
const placesRouter = require("./routes/places");
const bookingsRouter = require("./routes/bookings");

const app = express();
mongoose.connect(process.env.MONGO_URL);

app.use(express.json());
app.use(cookieParser());
app.use("/uploads", express.static(__dirname + "/uploads"));
app.use(
  cors({
    credentials: true,
    origin: process.env.ORIGIN,
  })
);

app.use("/", assetsRouter);
app.use("/", userRouter);
app.use("/places", placesRouter);
app.use("/bookings", bookingsRouter);

app.listen(4000);

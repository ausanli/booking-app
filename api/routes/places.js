const express = require("express");
const Place = require("../models/Place.js");
const jwt = require("jsonwebtoken");
const router = express.Router();
const jwtSecret = process.env.JWT_SECRET;

router.post("/", (req, res) => {
  const { token } = req.cookies;
  const {
    title,
    address,
    country,
    addedPhotos,
    description,
    perks,
    extraInfo,
    checkin,
    checkout,
    maxGuests,
    price,
  } = req.body;
  jwt.verify(token, jwtSecret, {}, async (error, userData) => {
    if (error) throw error;

    const placeDoc = await Place.create({
      owner: userData.id,
      title,
      address,
      country,
      photos: addedPhotos,
      description,
      perks,
      extraInfo,
      checkin,
      checkout,
      maxGuests,
      price,
    });
    res.json(placeDoc);
  });
});

router.put("/", async (req, res) => {
  const { token } = req.cookies;
  const {
    id,
    title,
    address,
    country,
    addedPhotos,
    description,
    perks,
    extraInfo,
    checkin,
    checkout,
    maxGuests,
    price,
  } = req.body;

  if (token) {
    jwt.verify(token, jwtSecret, {}, async (error, userData) => {
      if (error) throw error;
      const placeDoc = await Place.findById(id);
      if (userData.id === placeDoc.owner.toString()) {
        placeDoc.set({
          title,
          address,
          country,
          photos: addedPhotos,
          description,
          perks,
          extraInfo,
          checkin,
          checkout,
          maxGuests,
          price,
        });
        await placeDoc.save();
        res.json("updated");
      }
    });
  } else {
    res.json(null);
  }
});

router.get("/", async (req, res) => {
  const { minPrice, maxPrice, country } = req.query;
  const regex = new RegExp(country, "i");
  let filter = {};
  if (minPrice) {
    filter.price = { $gte: minPrice };
  }
  if (maxPrice) {
    filter.price = { ...filter.price, $lte: maxPrice };
  }
  if (country) {
    filter.country = regex;
  }
  const places = await Place.find(filter);
  res.json(places);
});

router.get("/:id", async (req, res) => {
  const { id } = req.params;
  res.json(await Place.findById(id));
});

module.exports = router;

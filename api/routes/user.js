const express = require("express");
const User = require("../models/User.js");
const jwt = require("jsonwebtoken");
const bcryptjs = require("bcryptjs");
const Place = require("../models/Place.js");
const router = express.Router();
const jwtSecret = process.env.JWT_SECRET;
const bcryptSalt = bcryptjs.genSaltSync(8);

router.post("/register", async (req, res) => {
  const { name, email, password } = req.body;
  try {
    const userDoc = await User.create({
      name,
      email,
      password: bcryptjs.hashSync(password, bcryptSalt),
    });
    res.json(userDoc);
  } catch (error) {
    res.status(422).json(error);
  }
});

router.post("/login", async (req, res) => {
  const { email, password } = req.body;

  const userDoc = await User.findOne({ email });
  if (userDoc) {
    const passwordCorrect = bcryptjs.compareSync(password, userDoc.password);
    if (passwordCorrect) {
      jwt.sign(
        { email: userDoc.email, id: userDoc._id },
        jwtSecret,
        {},
        (err, token) => {
          if (err) throw err;
          res.cookie("token", token).json(userDoc);
        }
      );
    } else {
      res.status(422).json("Wrong password");
    }
  } else {
    res.status(404).json("Not found");
  }
});

router.get("/profile", (req, res) => {
  const { token } = req.cookies;
  if (token) {
    jwt.verify(token, jwtSecret, {}, async (error, cookieData) => {
      if (error) throw error;
      const { name, email, _id } = await User.findById(cookieData.id);
      res.json({ name, email, _id });
    });
  } else {
    res.json(null);
  }
});

router.post("/logout", async (req, res) => {
  res.cookie("token", "").json(true);
});

router.get("/user-places", (req, res) => {
    const { token } = req.cookies;
    if (token) {
      jwt.verify(token, jwtSecret, {}, async (error, userData) => {
        if (error) throw error;
        const { id } = userData;
        res.json(await Place.find({ owner: id }));
      });
    } else {
      res.json(null);
    }
  });

module.exports = router;

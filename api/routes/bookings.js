const express = require("express");
const Booking = require("../models/Booking.js");
const Place = require("../models/Place.js");
const jwt = require("jsonwebtoken");
const router = express.Router();
const jwtSecret = process.env.JWT_SECRET;

const getUserDataFromReq = (req) => {
  return new Promise((resolve, reject) => {
    jwt.verify(req.cookies.token, jwtSecret, {}, async (error, userData) => {
      if (error) throw error;
      resolve(userData);
    });
  });
};

router.post("/", async (req, res) => {
  const userData = await getUserDataFromReq(req);
  const {
    placeId,
    checkin,
    checkout,
    numberOfGuests,
    name,
    email,
    phone,
    price,
  } = req.body;

  try {
    const existingBooking = await Booking.findOne({
      placeId,
      $or: [
        { checkin: { $lte: checkin }, checkout: { $gt: checkin } },
        { checkin: { $lt: checkout }, checkout: { $gte: checkout } },
        { checkin: { $gte: checkin }, checkout: { $lte: checkout } },
      ],
    });
    if (existingBooking) {
      return res
        .status(400)
        .json({
          error: "This place is already booked for the requested dates",
        });
    }
    const bookingDoc = await Booking.create({
      placeId,
      user: userData.id,
      checkin,
      checkout,
      numberOfGuests,
      name,
      email,
      phone,
      price,
    });
    res.json(bookingDoc);
  } catch (error) {
    res
      .status(500)
      .json({ error: "An error occurred while processing your request" });
  }
});

router.get("/", async (req, res) => {
  const userData = await getUserDataFromReq(req);
  res.json(await Booking.find({ user: userData.id }).populate("placeId"));
});

router.get("/search", async (req, res) => {
    const { q } = req.query;
    if (!q) {
      return res.status(400).json({ message: "Search query is required" });
    }
  
    try {
      const regex = new RegExp(q, "i");
      const places = await Place.find({ title: regex });
      res.json(places);
    } catch (error) {
      console.error(error);
      res.status(500).json({ message: error.message });
    }
  });

module.exports = router;

const express = require("express");
const router = express.Router();
const download = require("image-downloader");
const multer = require("multer");
const fs = require("fs");

router.post("/upload-by-link", async (req, res) => {
    const { link } = req.body;
    const newName = "photo" + Date.now() + ".jpg";
    try {
      await download.image({
        url: link,
        dest: __dirname + "/uploads/" + newName, // will be saved to /path/to/uploads/image.jpg
      });
      res.json(newName);
    } catch (error) {
      res.status(500).json(error.message);
    }
  });
  
  const photosMiddleware = multer({ dest: "uploads/" });
  
  router.post("/upload", photosMiddleware.array("photos", 12), (req, res) => {
    const uploadedFiles = [];
    for (let i = 0; i < req.files.length; i++) {
      const { path, originalname: originalName } = req.files[i];
      const parts = originalName.split(".");
      const extention = parts[parts.length - 1];
      const newPath = path + "." + extention;
      fs.renameSync(path, newPath);
      const newPathWithoutUploads = newPath.replace("uploads\\", "");
      const newPathWithoutSlash = newPathWithoutUploads.replace(path.sep);
      uploadedFiles.push(newPathWithoutSlash);
    }
    res.json(uploadedFiles);
  });

  module.exports = router;
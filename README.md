# Reservio - Booking App

This project is a booking app (inspired by Airbnb), which includes a frontend and a backend. The frontend is built with Vite+React, and the backend is a Node.js app using Express.js. This project allows users to register, login, view all places, create a new place, and make and see reservations for places. Mongo DB is used as persistent storage.


##  Installation

To run this project, you will need to have [Docker](https://www.docker.com/get-started) installed on your machine.

1.  Clone this repository to your local machine.
2.  Navigate to the root directory of the project in your terminal.
3.  Run the following command to start the Docker containers: `docker-compose up`

This will start the backend and frontend servers and launch the application. You can then access the application by opening your browser and going to `http://localhost`.

## Backend

The backend of this project is built with Node.js and Express.js. It includes endpoints for user authentication, creating, updating, and retrieving places, and making reservations.

To build the backend Docker image, navigate to the `api` directory and run the following command:

Copy code

`docker build -t ausanli/booking-api:1 .` 

## Frontend

The frontend of this project is built with React + Vite. It includes pages for user authentication, viewing all places, creating a new place, updating it, and making a reservation.

To build the frontend Docker image, navigate to the `client` directory and run the following command:

Copy code

`docker build -t ausanli/booking-client:2 .`

### NGINX

The frontend is served using NGINX

## Technologies Used

-   Node.js
-   Express.js
-   React
-   Vite
-   Docker
-   Docker Compose
- MongoDB
- Mongoose
- nginx

